var input = [];

require('fs').readFileSync('my.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
    input.push(line);
})
// require('fs').readFileSync('my.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
//     input.push(line);
// })

class Board {
    board = [];
    boardNumbers = [];
    marks = [];
    marksX = [];
    marksY = [];

    constructor(lines, size) {
        this.board = lines;
        lines.forEach(line => {
            line.forEach(n => {
                this.boardNumbers.push(n);
            })
        })
    }

    mark(number) {
        this.marks.push(number);
        this.board.forEach((line, y) => {
            line.forEach((num, x) => {
                if (num === number) {
                    this.marksX.push(x);
                    this.marksY.push(y);
                }
            })
        })
    }

    get isWinner() {
        const countsX = {};
        const countsY = {};

        for (const num of this.marksX) {
            countsX[num] = countsX[num] ? countsX[num] + 1 : 1;
        }
        for (const num of this.marksY) {
            countsY[num] = countsY[num] ? countsY[num] + 1 : 1;
        }

       return (Object.values(countsX).some(n => n >= 5)) || (Object.values(countsY).some(n => n >= 5));
    }

    score(actualNumber) {
        var numDiff = this.boardNumbers.filter(x => !this.marks.includes(x));
        var numDiffSum = 0;
        numDiff.forEach(n => {
            numDiffSum += parseInt(n);
        })
        return numDiffSum * actualNumber;
    }
}

var calls = [];
var boardSize = 5;
var newBoardLines = [];
var newBoardIndex = 0;
var boards = [];

// zpracuj input

input.forEach((i, index) => {
    if (index === 0) {
        calls = i.split(",");
        return;
    }
    if (i === "") {
        newBoardLines = [];
        newBoardIndex = 0;
        return;
    }

    newBoardLines.push(i.split(" ").filter(v => v));
    newBoardIndex++;

    if (newBoardIndex === boardSize) {
        boards.push(new Board(newBoardLines, boardSize));
    }
})

// vyvolavej
var isWinner = false;
var winnerCount = 0;
calls.forEach(callNumber => {
    if (isWinner) return;
    boards.forEach(board => {
        board.mark(callNumber);
    });
    boards.forEach(board => {
        if (board.isWinner) {
            boards.splice(boards.indexOf(board), 1);
        }
    });
})

