var foo = [];
var bar = [];

require('fs').readFileSync('test.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
    bar.push(line);
})
require('fs').readFileSync('my.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
    foo.push(line);
})

var depth = 0;
var aim = 0;
var horizontal = 0;

foo.forEach((i, index) => {
    var dir = i.split(" ")[0];
    var val = parseInt(i.split(" ")[1]);

    switch (dir) {
        case "forward":
            horizontal += val;
            depth += (val * aim);
            break;
        case "down":
            aim += val;
            break;
        case "up":
            aim -= val;
            break;
    }
})

console.log(depth, horizontal, depth * horizontal);
