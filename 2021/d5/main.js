var input = [];

require('fs').readFileSync('my.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
    input.push(line);
})
// require('fs').readFileSync('my.txt', 'utf-8').split(/\r?\n/).forEach(function (line) {
//     input.push(line);
// })

class Board {
    board = [];
    size = 0;

    constructor(size) {
        this.size = size;
        for (var y = 0; y < size; y++) {
            this.board.push([]);
            for (var x = 0; x < size; x++) {
                this.board[y].push(0);
            }
        }
    }

    mark(x1, y1, x2, y2) {
        var yDiff = y1 < y2 ? 1 : (y1 === y2 ? 0 : -1);
        var xDiff = x1 < x2 ? 1 : (x1 === x2 ? 0 : -1);

        if (y1 === y2) {
            for (var x = x1; true; x = x + xDiff) {
                this.board[y1][x]++;
                if (x === x2) break;
            }
        } else if (x1 === x2) {
            for (var y = y1; true; y = y + yDiff) {
                this.board[y][x1]++;
                if (y === y2) break;
            }
        } else {
            var x = x1;
            for (var y = y1; true; y = y + yDiff) {
                this.board[y][x]++;
                x = x + xDiff;
                if (y === y2) break;
            }
        }
    }

    getLargerThen(n) {
        var c = 0;

        for (var y = 0; y < this.size; y++) {
            for (var x = 0; x < this.size; x++) {
                if (this.board[y][x] > n) {
                    c++;
                }
            }
        }
        return c;
    }
}

var boardSize = 1000;
var board = new Board(boardSize);
// zpracuj input

input.forEach((i, index) => {
    var chords = i.split(" -> ");
    var point1 = chords[0].split(",").map(n => parseInt(n));
    var point2 = chords[1].split(",").map(n => parseInt(n));
    board.mark(point1[0], point1[1], point2[0], point2[1]);
})

console.log(board.board);
console.log(board.getLargerThen(1));
